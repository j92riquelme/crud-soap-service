package producto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BaseDeDatos {
    private Connection connection = null;
    private String driverBD = "org.postgresql.Driver";
    private String host = "jdbc:postgresql://localhost:5433/soapdb";
    private String user = "postgres";
    private String pass = "ROOT";

    public Connection conectarBD() throws SQLException {
        try {
            Class.forName(driverBD);
            connection = DriverManager.getConnection(host, user, pass);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BaseDeDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    public boolean insertarProducto(TipoProducto prod) throws SQLException {
        String sql = "INSERT INTO public.productos(nombre, precio, proveedor) VALUES (?, ?, ?);";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setString(1, prod.getNombre());
        st.setInt(2, prod.getPrecio());
        st.setString(3, prod.getProveedor());
        int nroRegistros = st.executeUpdate();
        st.close();
        if (nroRegistros == 0)
            return false;
        return true;
    }

    public List<TipoProducto> obtenerProductos() throws SQLException {
        List<TipoProducto> listado = new ArrayList<>();
        String sql = "SELECT id_producto, nombre, precio, proveedor FROM public.productos;";
        PreparedStatement st = connection.prepareStatement(sql);

        ResultSet rs = st.executeQuery();
        while (rs.next())
        {
            TipoProducto prod=new TipoProducto();
            prod.setCodigoProducto(rs.getInt(1));
            prod.setNombre(rs.getString(2));
            prod.setPrecio(rs.getInt(3));
            prod.setProveedor(rs.getString(4));
            listado.add(prod);
        }
        st.close();
        return listado;
    }

    public TipoProducto buscarProducto(int codigoProducto) throws SQLException {
        TipoProducto prod = new TipoProducto();
        String sql = "SELECT id_producto, nombre, precio, proveedor FROM public.productos WHERE id_producto = ?;";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, codigoProducto);

        ResultSet rs = st.executeQuery();
        while (rs.next())
        {
            prod.setCodigoProducto(rs.getInt(1));
            prod.setNombre(rs.getString(2));
            prod.setPrecio(rs.getInt(3));
            prod.setProveedor(rs.getString(4));
        }
        st.close();
        return prod;
    }

    public boolean eliminarProducto(int codigoProducto) throws SQLException {
        String sql = "DELETE FROM public.productos WHERE id_producto = ?;";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setInt(1, codigoProducto);
        int cantRegistros = st.executeUpdate();
        st.close();
        if (cantRegistros == 0)
            return false;
        return true;
    }

    public boolean actualizarProducto(TipoProducto prod) throws SQLException {
        String sql = "UPDATE public.productos SET nombre=?, precio=?, proveedor=? WHERE id_producto=?;";
        PreparedStatement st = connection.prepareStatement(sql);
        st.setString(1, prod.getNombre());
        st.setInt(2, prod.getPrecio());
        st.setString(3, prod.getProveedor());
        st.setInt(4, prod.getCodigoProducto());
        int nroRegistros = st.executeUpdate();
        st.close();
        if (nroRegistros == 0)
            return false;
        return true;
    }

    public void cerrarBD() throws SQLException {
        if (connection != null)
            connection.close();
    }
}
