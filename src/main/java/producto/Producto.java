package producto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface Producto {

    @WebMethod(operationName = "agregar")
    @WebResult(name="Respuesta")
    String agregarProducto(@WebParam(name="producto") TipoProducto producto);

    @WebMethod(operationName = "listar")
    @WebResult(name="Productos")
    List<TipoProducto> listarProducto();

    @WebMethod(operationName = "buscar")
    @WebResult(name="Producto")
    TipoProducto buscarProducto(@WebParam(name="codigo") int cod_producto);

    @WebMethod(operationName = "eliminar")
    @WebResult(name="Respuesta")
    String eliminarProducto(@WebParam(name="codigoProducto") int cod_producto);

    @WebMethod(operationName = "actualizar")
    @WebResult(name="Respuesta")
    String actualizarProducto(@WebParam(name="producto") TipoProducto producto);
}