
package producto;

import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService(serviceName = "producto")
public class ProductoService implements Producto {
    private BaseDeDatos dbProducto = new BaseDeDatos();

    @Override
    public String agregarProducto(TipoProducto producto) {
        try {
            String mensaje;
            dbProducto.conectarBD();
            if (dbProducto.insertarProducto(producto))
                mensaje = "Producto agregado correctamente";
            else
                mensaje = "Error al agregar el producto";
            dbProducto.cerrarBD();
            return mensaje;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<TipoProducto> listarProducto() {
        try {
            List<TipoProducto> listaProductos;
            dbProducto.conectarBD();
            listaProductos = dbProducto.obtenerProductos();
            dbProducto.cerrarBD();
            return listaProductos;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public TipoProducto buscarProducto(int cod_producto) {
        TipoProducto producto;
        try {
            dbProducto.conectarBD();
            producto = dbProducto.buscarProducto(cod_producto);
            dbProducto.cerrarBD();
            return producto;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public String eliminarProducto(int cod_producto) {
        try {
            String mensaje;
            dbProducto.conectarBD();
            TipoProducto tmpProducto = dbProducto.buscarProducto(cod_producto);
            if (dbProducto.eliminarProducto(cod_producto))
                mensaje = "Producto eliminado correctamente -> " + tmpProducto.toString() ;
            else
                mensaje = "Error al eliminar el producto con id " + cod_producto;
            dbProducto.cerrarBD();
            return mensaje;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String actualizarProducto(TipoProducto producto) {
        try {
            String mensaje;
            dbProducto.conectarBD();
            if (dbProducto.actualizarProducto(producto)) {
                TipoProducto tmpProducto = dbProducto.buscarProducto(producto.getCodigoProducto());
                mensaje = "Producto actualizado: " + tmpProducto.toString();
            } else {
                mensaje = "Error al actualizar el producto: " + producto.toString();
            }
            dbProducto.cerrarBD();
            return mensaje;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
