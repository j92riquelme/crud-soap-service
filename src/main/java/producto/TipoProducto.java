package producto;

import javax.xml.bind.annotation.XmlElement;

public class TipoProducto {
    private int codigoProducto;
    private String nombre;
    private int precio;
    private String proveedor;

    @XmlElement(name = "codigoProducto", required=false)
    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    @XmlElement(name = "nombreProducto", required=true)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlElement(name = "precioProducto",required=true)
    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @XmlElement(name = "nombreProveedor", required=true)
    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public String toString() {
        return  "codigoProducto:" + codigoProducto +
                ", nombre:" + nombre +
                ", precio:" + precio +
                ", proveedor:" + proveedor ;
    }
}