package operacion;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface Calculadora {
	@WebMethod
	public double operacion(@WebParam(name = "tipoOperacion") int operacion, @WebParam(name = "primerNum") int valor1, @WebParam(name = "segundoNum")int valor2);

}