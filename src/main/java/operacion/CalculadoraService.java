package operacion;

import javax.jws.WebService;

@WebService(serviceName = "calculadora")
public class CalculadoraService implements Calculadora {


	@Override
	public double operacion(int operacion, int valor1, int valor2) {
		double resultado = 0;
		switch (operacion) {
			case 1:
				resultado = valor1 + valor2;
				break;
			case 2:
				resultado = valor1 - valor2;
				break;
			case 3:
				resultado = valor1 * valor2;
				break;
			case 4:
				resultado = valor1 / valor2;
				break;
			default:
				break;
		}
		return resultado;
	}
}
