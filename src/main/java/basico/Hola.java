package basico;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Hola {
    private String message = new String("Hola, ");

    @WebMethod
    public String sayHello(String name) {
        return message + name + ".";
    }
}